resource "mongodbatlas_project_ip_access_list" "default" {
  for_each = { for rule in var.atlas_fw_default : rule.name => rule }

  project_id = mongodbatlas_project.main.id
  cidr_block = each.value.ips_cidrs
  comment    = each.value.name
}

resource "mongodbatlas_project_ip_access_list" "apps" {
  for_each = { for rule in var.atlas_fw_apps : rule.name => rule }

  project_id = mongodbatlas_project.main.id
  cidr_block = each.value.ips_cidrs
  comment    = each.value.name
}

resource "mongodbatlas_project_ip_access_list" "users" {
  for_each = { for rule in var.atlas_fw_users : rule.name => rule }

  project_id = mongodbatlas_project.main.id
  cidr_block = each.value.ips_cidrs
  comment    = each.value.name
}