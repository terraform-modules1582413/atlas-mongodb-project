resource "mongodbatlas_project" "main" {
  name   = module.name.pretty
  org_id = var.atlas_org_id

  is_collect_database_specifics_statistics_enabled = false
  is_data_explorer_enabled                         = true
  is_performance_advisor_enabled                   = true
  is_realtime_performance_panel_enabled            = true
  is_schema_advisor_enabled                        = true
}