variable "gcp_regions_mapping" {
  default = {
    "europe-central2" = "EUROPE_CENTRAL_2"
  }
}
locals {
  gcp_region = lookup(var.gcp_regions_mapping, var.gcp_region)
}

resource "mongodbatlas_network_container" "main" {
  project_id       = mongodbatlas_project.main.id
  atlas_cidr_block = "172.17.0.0/24"
  provider_name    = "GCP"
  regions          = [local.gcp_region]
}

resource "mongodbatlas_network_peering" "main" {
  project_id     = mongodbatlas_project.main.id
  container_id   = mongodbatlas_network_container.main.container_id
  provider_name  = "GCP"
  gcp_project_id = var.gcp_network_project_id
  network_name   = var.gcp_network_name

  depends_on = [mongodbatlas_network_container.main]
}

resource "google_compute_network_peering" "peering" {
  name         = "${module.name.short}-peering-${module.name.appendix}"
  network      = var.gcp_network_self_link
  peer_network = "https://www.googleapis.com/compute/v1/projects/${mongodbatlas_network_peering.main.atlas_gcp_project_id}/global/networks/${mongodbatlas_network_peering.main.atlas_vpc_name}"

  depends_on = [mongodbatlas_network_container.main]
}