terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.59.0"
    }
    mongodbatlas = {
      source  = "mongodb/mongodbatlas"
      version = "~> 1.8.2"
    }
  }
}
