variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "project_name" {
  type = string
}

variable "gcp_region" {
  type = string
}
variable "gcp_network_project_id" {
  type = string
}
variable "gcp_network_name" {
  type = string
}
variable "gcp_network_self_link" {
  type = string
}

variable "atlas_org_id" {
  type = string
}
variable "atlas_project_name" {
  type = string
}

variable "atlas_fw_default" {
  type = list(object({
    name      = string
    ips_cidrs = string
  }))
  default = []
}
variable "atlas_fw_apps" {
  type = list(object({
    name      = string
    ips_cidrs = string
  }))
  default = []
}
variable "atlas_fw_users" {
  type = list(object({
    name      = string
    ips_cidrs = string
  }))
  default = []
}
